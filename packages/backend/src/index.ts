console.log("Debug: Loading backend/index");

export function ValidateUserName(name: string): boolean {
  return (name !== undefined && name.length > 8);
}
